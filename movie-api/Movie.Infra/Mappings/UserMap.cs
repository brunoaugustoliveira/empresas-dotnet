﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Movie.Domain.Entities;

namespace Movie.Infra.Data.Mappings
{
    public class UserMap : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(m => m.FullName)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(m => m.UserName)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(m => m.Password)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(m => m.RegisterDate)
                .IsRequired()
                .HasColumnType("datetime");

            builder.Property(m => m.Role)
                .IsRequired()
                .HasMaxLength(20);

            builder.Property(m => m.Active)
                .IsRequired();

            builder.ToTable("User");
        }
    }
}

﻿using Movie.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Movie.Infra.Data.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        Task<bool> GetExistingUserByUsernameAsync(string username);
        Task<UserAuth> LogInAsync(string username, string password);
        Task<bool> CheckIfNewUserAlreadyExists(int id, string username);
        Task DisableAsync(int id);
        Task<IEnumerable<User>> GetActiveUsers(int? take, int? skip);
    }
}

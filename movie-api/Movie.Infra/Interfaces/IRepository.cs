﻿using System.Threading.Tasks;

namespace Movie.Infra.Data.Interfaces
{
    public interface IRepository<TEntity>
    {
        Task<TEntity> CreateAsync(TEntity entity);
        Task DeleteAsync(int id);
        Task<TEntity> UpdateAsync(TEntity entity);
        Task SaveChangesAsync();
        Task<TEntity> GetByIdAsync(int id);
    }
}

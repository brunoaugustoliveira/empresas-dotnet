﻿using Microsoft.EntityFrameworkCore;
using Movie.Domain.Entities;
using Movie.Infra.Data.Context;
using Movie.Infra.Data.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace Movie.Infra.Data.Repositories
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity<TEntity>
    {
        protected MovieContext Context;
        protected DbSet<TEntity> DbSet;

        public Repository(MovieContext context)
        {
            Context = context;
            DbSet = context.Set<TEntity>();
        }

        public virtual async Task<TEntity> CreateAsync(TEntity entity)
        {
            DbSet.Add(entity);
            await SaveChangesAsync();
            return entity;
        }

        public async virtual Task DeleteAsync(int id)
        {
            DbSet.Remove(await DbSet.FindAsync(id));
        }


        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            DbSet.Update(entity);
            await SaveChangesAsync();

            return entity;
        }

        public async Task SaveChangesAsync()
        {
            await Context.SaveChangesAsync();
        }

        public async Task<TEntity> GetByIdAsync(int id)
        {
            return await DbSet.Where(entity => entity.Id == id).FirstOrDefaultAsync();
        }
    }
}

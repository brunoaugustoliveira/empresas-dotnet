﻿using Microsoft.EntityFrameworkCore;
using Movie.Domain.Entities;
using Movie.Infra.Data.Context;
using Movie.Infra.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Movie.Infra.Data.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(MovieContext context) : base(context) { }

        public async Task<bool> GetExistingUserByUsernameAsync(string username)
        {
            return await DbSet.AnyAsync(user => user.UserName == username.ToLower());
        }

        public async Task<UserAuth> LogInAsync(string username, string password)
        {
            return await DbSet.Where(user => user.UserName == username && user.Password == password && user.Active)
                            .Select(user => new UserAuth
                            {
                                Id = user.Id,
                                FullName = user.FullName,
                                UserName = user.UserName,
                                Role = user.Role
                            })
                            .FirstOrDefaultAsync();
        }

        public async Task<bool> CheckIfNewUserAlreadyExists(int id, string username)
        {
            return await DbSet.AnyAsync(user => user.Id != id && user.UserName == username);
        }

        public async Task DisableAsync(int id)
        {
            var user = await DbSet.Where(user => user.Id == id).FirstOrDefaultAsync();
            user.DisableUser();
            await UpdateAsync(user);
        }

        public async Task<IEnumerable<User>> GetActiveUsers(int? take, int? skip)
        {
            var query = DbSet.Where(user => user.Role != "administrator" && user.Active).OrderBy(user => user.FullName).AsQueryable();

            if(take != null && skip != null)
            {
                query = query.Skip((int)skip).Take((int)take);
            }

            return await query.ToListAsync();
        }
    }
}

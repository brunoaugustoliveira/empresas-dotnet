﻿using Microsoft.EntityFrameworkCore;
using Movie.Domain.Entities;
using Movie.Infra.Data.Mappings;

namespace Movie.Infra.Data.Context
{
    public class MovieContext : DbContext
    {
        public MovieContext(DbContextOptions<MovieContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserMap());
        }
    }
}

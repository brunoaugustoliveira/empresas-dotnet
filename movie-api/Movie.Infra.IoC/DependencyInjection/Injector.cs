﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Movie.Infra.IoC.DependencyInjection.Database;
using Movie.Infra.IoC.DependencyInjection.Services;
using Movie.Infra.IoC.DependencyInjection.AutoMapper;
using Movie.Infra.IoC.DependencyInjection.Repositories;
using Movie.Infra.IoC.DependencyInjection.Secutiry;
using Movie.Infra.IoC.DependencyInjection.AppSettings;

namespace Movie.Infra.IoC.DependencyInjection
{
    public static class Injector
    {
        public static void RegisterDependencyInjector(this IServiceCollection services, IConfiguration configuration)
        {
            services.RegisterServices();
            services.RegisterRepositories();
            services.RegisterAutoMapper();
            services.RegisterSecurity();
            services.RegisterAppSettings(configuration);
            services.RegisterSecurityToken(configuration);
            services.RegisterDatabase(configuration);
        }
    }
}

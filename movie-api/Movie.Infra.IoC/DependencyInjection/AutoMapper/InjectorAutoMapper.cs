﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Movie.Application.AutoMapper;

namespace Movie.Infra.IoC.DependencyInjection.AutoMapper
{
    public static class InjectorAutoMapper
    {
        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new ViewModelToDomain());
                mc.AddProfile(new DomainToViewModel());
                mc.AllowNullCollections = true;
                mc.AllowNullDestinationValues = true;
                mc.ValidateInlineMaps = false;
            });

            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}

﻿using Microsoft.Extensions.DependencyInjection;
using Movie.Infra.Data.Interfaces;
using Movie.Infra.Data.Repositories;

namespace Movie.Infra.IoC.DependencyInjection.Repositories
{
    public static class InjectorRepositories
    {
        public static void RegisterRepositories(this IServiceCollection services)
        {
            services.AddScoped<IUserRepository, UserRepository>();
        }
    }
}

﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Movie.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace Movie.Infra.IoC.DependencyInjection.Database
{
    public static class InjectorDatabase
    {
        public static void RegisterDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<MovieContext>(options => 
                    options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));
        }
    }
}

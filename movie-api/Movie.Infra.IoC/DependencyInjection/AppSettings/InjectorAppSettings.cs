﻿using Microsoft.Extensions.DependencyInjection;
using Movie.Infra.Security.Entities;
using Microsoft.Extensions.Configuration;

namespace Movie.Infra.IoC.DependencyInjection.AppSettings
{
    public static class InjectorAppSettings
    {
        public static void RegisterAppSettings(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<Token>(
                configuration.GetSection(nameof(Token)));
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace movie_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MovieController
    {
        /// <summary>
        /// Cria um filme
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">Filme criado com sucesso</response>
        [HttpPost]
        public async Task<IActionResult> Create(object movie)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Movie.Application.Interfaces;
using Movie.Application.ViewModels;
using movie_api.Configuration;
using System;
using System.Threading.Tasks;

namespace movie_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Cria um usuário
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">Usuário criado com sucesso</response>
        [HttpPost]
        [Authorize(Roles = AuthRole.AllUsers)]
        public async Task<IActionResult> Create(UserViewModel user)
        {
            try
            {
                var userCreated = await _userService.CreateAsync(user);
                return Ok(userCreated);
            } 
            catch(Exception e)
            {
                return BadRequest(new { Message = e.Message, StackTrace = e.StackTrace });
            }
        }

        /// <summary>
        /// Atualiza um usuário
        /// </summary>
        /// <param name="user">
        /// Modelo que representa o usuário
        /// </param>
        /// <returns></returns>
        /// <response code="200">Usuário atualizado com sucesso</response>
        [HttpPut]
        [Authorize(Roles = AuthRole.AllUsers)]
        public async Task<IActionResult> Update(UserViewModel user)
        {
            try
            {
                var userUpdated = await _userService.UpdateAsync(user);
                return Ok(userUpdated);
            }
            catch (Exception e)
            {
                return BadRequest(new { Message = e.Message, StackTrace = e.StackTrace });
            }
        }

        /// <summary>
        /// Desativa um usuário
        /// </summary>
        /// <param name="id">
        /// Id do usuário
        /// </param>
        /// <returns></returns>
        /// <response code="200">Usuário desativado com sucesso</response>
        [HttpDelete("{id}")]
        [Authorize(Roles = AuthRole.AllUsers)]
        public async Task Delete(int id)
        {
            try
            {
                await _userService.DeleteAsync(id);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Obtém lista de usuários não administradores que estão ativo
        /// </summary>
        /// <param name="take">
        /// Parâmetro para definir o máximo de registro obtido a cada requisição
        /// </param>
        /// <param name="skip">
        /// Parâmetro para definir quantos registros deverão ser desconsiderados ao realizar a busca
        /// </param>
        /// <returns></returns>
        /// <response code="200">Usuários obtidos com sucesso</response>
        [HttpGet]
        [Route("users")]
        [Authorize(Roles = AuthRole.Administrator)]
        public async Task<IActionResult> GetActiveUsers(int? take, int? skip)
        {
            try
            {
                var users = await _userService.GetActiveUsers(take, skip);
                return Ok(users);
            }
            catch (Exception e)
            {
                return BadRequest(new { Message = e.Message, StackTrace = e.StackTrace });
            }
        }
    }
}

﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Movie.Application.Interfaces;
using Movie.Application.ViewModels;
using System;
using System.Threading.Tasks;

namespace movie_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        /// <summary>
        /// Realiza a autenticação do usuário
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">LogIn realizado com sucesso</response>
        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<IActionResult> LogIn(AuthViewModel userAuthentication)
        {
            try
            {
                string token = await _authService.LogInAsync(userAuthentication);
                Response.Headers.Add("x-access-token", token);
                return Ok(true);
            }
            catch (Exception e)
            {
                return BadRequest(new { Message = e.Message });
            }
        }
    }
}

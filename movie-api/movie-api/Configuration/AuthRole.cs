﻿namespace movie_api.Configuration
{
    internal static class AuthRole
    {
        internal const string Administrator = "administrator";
        internal const string User = "user";
        internal const string AllUsers = Administrator + "," + User;
    }
}

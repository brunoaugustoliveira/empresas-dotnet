﻿namespace Movie.Infra.Security.Interfaces
{
    public interface IEncryptService
    {
        string Encrypt(string value);
    }
}

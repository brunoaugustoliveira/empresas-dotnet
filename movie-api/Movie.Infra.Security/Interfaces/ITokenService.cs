﻿using Movie.Infra.Security.Entities;

namespace Movie.Infra.Security.Interfaces
{
    public interface ITokenService
    {
        string GenerateToken(TokenUser user);
    }
}

﻿namespace Movie.Infra.Security.Entities
{
    public class Token
    {
        public string Secret { get; set; }
    }
}

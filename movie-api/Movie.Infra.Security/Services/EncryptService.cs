﻿using Movie.Infra.Security.Interfaces;
using System.Security.Cryptography;
using System.Text;

namespace Movie.Infra.Security.Services
{
    public class EncryptService : IEncryptService
    {
        public string Encrypt(string value)
        {
            byte[] data = Encoding.ASCII.GetBytes(value);
            data = new SHA256Managed().ComputeHash(data);
            return Encoding.ASCII.GetString(data);
        }
    }
}

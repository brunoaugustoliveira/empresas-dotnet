﻿using AutoMapper;
using Movie.Application.Interfaces;
using Movie.Application.Utils.Exceptions;
using Movie.Application.ViewModels;
using Movie.Domain.Entities;
using Movie.Infra.Data.Interfaces;
using Movie.Infra.Security.Entities;
using Movie.Infra.Security.Interfaces;
using System.Threading.Tasks;

namespace Movie.Application.Services
{
    public class AuthService : IAuthService
    {
        private readonly IEncryptService _encryptService;
        private readonly ITokenService _tokenService;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public AuthService(IEncryptService encryptService, ITokenService tokenService, IUserRepository userRepository, IMapper mapper)
        {
            _encryptService = encryptService;
            _tokenService = tokenService;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<string> LogInAsync(AuthViewModel userLogin)
        {
            if(!string.IsNullOrEmpty(userLogin.UserName) && !string.IsNullOrEmpty(userLogin.Password))
            {
                string password = _encryptService.Encrypt(userLogin.Password);
                var user = await _userRepository.LogInAsync(userLogin.UserName, password);

                if(user is null)
                {
                    throw new UserNotFoundException();
                }

                var userToken = Map<TokenUser, UserAuth>(user);
                return _tokenService.GenerateToken(userToken);
            }
            else
            {
                throw new UserNotFoundException();
            }
        }

        private A Map<A, B>(B model)
            where A : class
            where B : class
        {
            return _mapper.Map<A>(model);
        }
    }
}

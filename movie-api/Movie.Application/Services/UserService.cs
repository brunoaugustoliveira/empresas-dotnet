﻿using AutoMapper;
using Movie.Application.Interfaces;
using Movie.Application.Utils.Exceptions;
using Movie.Application.ViewModels;
using Movie.Domain.Entities;
using Movie.Infra.Data.Interfaces;
using Movie.Infra.Security.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Movie.Application.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IEncryptService _encryptService;
        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, IEncryptService encryptService, IMapper mapper)
        {
            _userRepository = userRepository;
            _encryptService = encryptService;
            _mapper = mapper;
        }

        public async Task<UserViewModel> CreateAsync(UserViewModel userViewModel)
        {
            if (!await _userRepository.GetExistingUserByUsernameAsync(userViewModel?.UserName))
            {

                var user = await HandleUser(userViewModel);
                var userCreated = await _userRepository.CreateAsync(user);

                return Map<UserViewModel, User>(userCreated);
            }

            throw new ExistingUserException();
        }

        public async Task DeleteAsync(int id)
        {
            await _userRepository.DisableAsync(id);

        }

        public async Task<UserViewModel> UpdateAsync(UserViewModel userViewModel)
        {
            if(!await _userRepository.CheckIfNewUserAlreadyExists(userViewModel.Id, userViewModel.UserName))
            {
                var user = await HandleUser(userViewModel);
                var userUpdated = await _userRepository.UpdateAsync(user);

                return Map<UserViewModel, User>(userUpdated);
            }

            throw new ExistingUserException();
        }

        public A Map<A, B>(B model)
            where A : class
            where B : class
        {
            return _mapper.Map<A>(model);
        }

        private async Task<User> HandleUser(UserViewModel userViewModel)
        {
            var user = Map<User, UserViewModel>(userViewModel);
            user.CheckIsValid();
            user.SetRegisterDate();
            user.Password = _encryptService.Encrypt(user.Password);

            return user;
        }

        public async Task<IEnumerable<UserViewModel>> GetActiveUsers(int? take, int? skip)
        {
            var users = await _userRepository.GetActiveUsers(take, skip);
            return Map<IEnumerable<UserViewModel>, IEnumerable<User>>(users);
        }
    }
}

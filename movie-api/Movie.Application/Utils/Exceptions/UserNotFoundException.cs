﻿using System;

namespace Movie.Application.Utils.Exceptions
{
    public class UserNotFoundException : Exception
    {
        public UserNotFoundException(string message = "Incorrect username or password") : base(message) { }
    }
}

﻿using System;

namespace Movie.Application.Utils.Exceptions
{
    public class ExistingUserException : Exception
    {
        public ExistingUserException(string message = "The informed user already exists") : base(message) { }
    }
}

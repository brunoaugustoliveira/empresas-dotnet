﻿using System.Threading.Tasks;

namespace Movie.Application.Interfaces
{
    public interface IService<T> where T : class
    {
        Task<T> CreateAsync(T model);
        Task DeleteAsync(int id);
        Task<T> UpdateAsync(T model);
        A Map<A, B>(B model)
            where A : class
            where B : class;
    }
}

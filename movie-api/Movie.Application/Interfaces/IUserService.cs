﻿using Movie.Application.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Movie.Application.Interfaces
{
    public interface IUserService : IService<UserViewModel>
    {
        Task<IEnumerable<UserViewModel>> GetActiveUsers(int? take, int? skip);
    }
}

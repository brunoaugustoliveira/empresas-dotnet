﻿using Movie.Application.ViewModels;
using System.Threading.Tasks;

namespace Movie.Application.Interfaces
{
    public interface IAuthService
    {
        Task<string> LogInAsync(AuthViewModel userAuthentication);
    }
}

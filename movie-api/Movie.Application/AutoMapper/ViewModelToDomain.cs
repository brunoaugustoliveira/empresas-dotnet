﻿using AutoMapper;
using Movie.Application.ViewModels;
using Movie.Domain.Entities;

namespace Movie.Application.AutoMapper
{
    public class ViewModelToDomain : Profile
    {
        public ViewModelToDomain()
        {
            CreateMap<UserViewModel, User>();
        }
    }
}

﻿using AutoMapper;
using Movie.Application.ViewModels;
using Movie.Domain.Entities;

namespace Movie.Application.AutoMapper
{
    public class DomainToViewModel : Profile
    {
        public DomainToViewModel()
        {
            CreateMap<User, UserViewModel>()
                .ForMember(dest => dest.Password, sc => sc.Ignore());
        }
    }
}

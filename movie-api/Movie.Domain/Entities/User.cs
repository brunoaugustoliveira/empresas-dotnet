﻿using FluentValidation;
using Movie.Domain.Entities.Validators.Helpers;
using System;

namespace Movie.Domain.Entities
{
    public class User : Entity<User>
    {
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public bool Active { get; private set; } = true;
        public DateTime RegisterDate { get; private set; }

        public void SetRegisterDate()
        {
            RegisterDate = DateTime.Now;
        }

        public void ActiveUser()
        {
            Active = true;
        }

        public void DisableUser()
        {
            Active = false;
        }

        public override void CheckIsValid()
        {
            Validator.RuleFor(u => u.FullName)
                .NotEmpty()
                .NotNull()
                .WithMessage(MessageHelper.RequiredField("FullName"));

            Validator.RuleFor(u => u.UserName)
                .NotEmpty()
                .NotNull()
                .WithMessage(MessageHelper.RequiredField("UserName"));

            Validator.RuleFor(u => u.Password)
                .NotEmpty()
                .NotNull()
                .WithMessage(MessageHelper.RequiredField("Password"));

            Validator.RuleFor(u => u.Role)
                .NotEmpty()
                .NotNull()
                .WithMessage(MessageHelper.RequiredField("Role"))
                .Must(u => u == "user" || u == "administrator")
                .WithMessage("Profile must be administrator or user.");

            Validate(this);
        }
    }
}

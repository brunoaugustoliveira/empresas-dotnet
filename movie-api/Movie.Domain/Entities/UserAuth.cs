﻿namespace Movie.Domain.Entities
{
    public class UserAuth
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Role { get; set; }
    }
}

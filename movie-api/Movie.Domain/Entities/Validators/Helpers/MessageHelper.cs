﻿namespace Movie.Domain.Entities.Validators.Helpers
{
    public static class MessageHelper
    {
        public static string RequiredField(string fieldName)
        {
            return $"The {fieldName} field is required";
        }
    }
}

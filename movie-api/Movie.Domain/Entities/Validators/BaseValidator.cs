﻿using FluentValidation;
using FluentValidation.Results;
using System;
using System.Linq;

namespace Movie.Domain.Entities.Validators
{
    public class BaseValidator<T> : AbstractValidator<T> where T : class
    {
        public override ValidationResult Validate(ValidationContext<T> context)
        {
            var result = base.Validate(context);

            if(result?.Errors?.Count() > 0)
            {
                throw new Exception(string.Join("; ", result.Errors));
            }

            return result;
        }
    }
}

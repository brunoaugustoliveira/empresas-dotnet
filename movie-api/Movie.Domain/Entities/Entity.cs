﻿using FluentValidation;
using FluentValidation.Results;
using Movie.Domain.Entities.Validators;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Movie.Domain.Entities
{
    public abstract class Entity<TEntity> where TEntity : Entity<TEntity>
    {
        public int Id { get; set; }

        [JsonIgnore]
        [NotMapped]
        public BaseValidator<TEntity> Validator { get; protected set; }

        public Entity()
        {
            Validator = new BaseValidator<TEntity>();
        }

        public abstract void CheckIsValid();

        public ValidationResult Validate(TEntity entity)
        {
            var context = new ValidationContext<TEntity>(entity);
            return Validator.Validate(context);
        }
    }
}
